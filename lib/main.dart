import 'package:flutter/material.dart';
import 'package:nomadio_app/providers/auth.dart';
import 'package:nomadio_app/providers/cart.dart';
import 'package:nomadio_app/providers/orders.dart';
import 'package:nomadio_app/providers/product.dart';
import 'package:nomadio_app/screens/auth_screen.dart';
import 'package:nomadio_app/screens/cart_screen.dart';
import 'package:nomadio_app/screens/edit_product_screen.dart';
import 'package:nomadio_app/screens/orders_screen.dart';
import 'package:nomadio_app/screens/user_products_screen.dart';
import 'package:provider/provider.dart';
import 'package:nomadio_app/home_screen.dart';
import 'package:nomadio_app/screens/product_detail_screen.dart';
import 'package:nomadio_app/screens/products_overview_screen.dart';
import './providers/products.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider.value(
            value: Auth(),
          ),
          ChangeNotifierProxyProvider<Auth, Products>(
            create: (_) => Products('', '', []),
            update: (ctx, auth, previousProducts) => Products(
                auth.token ?? '',
                auth.userId,
                previousProducts == null ? [] : previousProducts.items),
          ),
          ChangeNotifierProvider(
            create: (ctx) => Cart(),
          ),
          ChangeNotifierProxyProvider<Auth, Orders>(
            create: (_) => Orders('', '', []),
            update: (ctx, auth, previuosOrder) => Orders(
              auth.token ?? '',
              auth.userId,
              previuosOrder == null ? [] : previuosOrder.orders,
            ),
          ),
        ],
        child: Consumer<Auth>(
          builder: (ctx, auth, _) => MaterialApp(
              title: 'Flutter Demo',
              theme: ThemeData(
                primarySwatch: Colors.blueGrey,
              ),
              home: auth.isAuth ? ProductsOverviewScreen() : AuthScreen(),
              debugShowCheckedModeBanner: false,
              routes: {
                ProductDetailScreen.routeName: (ctx) => ProductDetailScreen(),
                CartScreen.routeName: (ctx) => CartScreen(),
                OrdersScreen.routeName: (ctx) => OrdersScreen(),
                UserProductsScreen.routeName: (ctx) => UserProductsScreen(),
                EditProductScreen.routeName: (ctx) => EditProductScreen()
              }),
        ));
  }
}
