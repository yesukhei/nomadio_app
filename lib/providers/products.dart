import 'package:flutter/cupertino.dart';
import 'package:nomadio_app/models/http_exception.dart';
import './product.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Products with ChangeNotifier {
  List<Product> _items = [
    // Product(
    //   id: 'p1',
    //   title: 'Хөвсгөл аялал',
    //   description: 'Хөвсгөлийн тайгын аялал',
    //   price: 29.99,
    //   imageUrl: 'https://cdn3.shoppy.mn/spree/images/1018315/large/b1000.jpg',
    // ),
    // Product(
    //   id: 'p2',
    //   title: 'Алтай таван богд',
    //   description: 'Алтай таван богдын аялал',
    //   price: 59.99,
    //   imageUrl: 'https://dnn.mn/media/dnn/content/20184/04-02.jpg',
    // ),
    // Product(
    //   id: 'p3',
    //   title: 'Тэрэлж',
    //   description: 'Тэрэлж аялал',
    //   price: 19.99,
    //   imageUrl:
    //       'https://content.ikon.mn/news/2017/7/18/00937d_tumblr_m8lhzoQsMS1qmvijuo1_1280_x974.jpg',
    // ),
    // Product(
    //   id: 'p4',
    //   title: 'Архангай',
    //   description: 'Архангайн аялал',
    //   price: 49.99,
    //   imageUrl: 'https://mapio.net/images-p/17543314.jpg',
    // ),
  ];
  // var _showFavoritesOnly = false;
  final String authToken;
  final String userId;
  Products(this.authToken, this.userId, this._items);

  List<Product> get items {
    // if (_showFavoritesOnly) {
    //   return _items.where((prodItem) => prodItem.isFavorite).toList();
    // }
    return [..._items];
  }

  List<Product> get favoriteItems {
    return _items.where((prodItem) => prodItem.isFavorite).toList();
  }

  List<Product> searchItems(String input) {
    return _items
        .where((prod) => prod.location.toLowerCase().contains(input))
        .toList();
  }

  List<Product> get internalItems {
    return _items
        .where((prod) => prod.classification.contains("Dotood"))
        .toList();
  }

  List<Product> get externalItems {
    return _items
        .where((prod) => prod.classification.contains("Gadaad"))
        .toList();
  }

  Product findById(String id) {
    return _items.firstWhere((prod) => prod.id == id);
  }

  // void showFavoritesOnly() {
  //   _showFavoritesOnly = true;
  //   notifyListeners();
  // }

  // void showAll() {
  //   _showFavoritesOnly = false;
  //   notifyListeners();
  // }
  Future<void> fetchAndSetProducts([bool filterByUser = false]) async {
    final filterString =
        filterByUser ? 'orderBy="creatorId"&equalTo="$userId"' : '';
    var url =
        'https://nomadio-19725-default-rtdb.asia-southeast1.firebasedatabase.app/products.json?auth=$authToken&$filterString';
    try {
      final response = await http.get(Uri.parse(url));
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
      print("error");
      print(extractedData);
      if (extractedData == null) {
        return;
      }
      url =
          'https://nomadio-19725-default-rtdb.asia-southeast1.firebasedatabase.app/userFavorites/$userId.json?auth=$authToken';
      final favoriteResponse = await http.get(Uri.parse(url));
      final favoriteData = json.decode(favoriteResponse.body);
      final List<Product> loadedProduct = [];
      extractedData.forEach((prodId, prodData) {
        loadedProduct.add(Product(
            id: prodId,
            title: prodData['title'],
            description: prodData['description'],
            imageUrl: prodData['imageUrl'],
            price: prodData['price'],
            isFavorite:
                favoriteData == null ? false : favoriteData[prodId] ?? false,
            classification: prodData['classification'],
            vehicle: prodData['vehicle'],
            location: prodData['location'],
            day: prodData['day'],
            peopleCount: prodData['peopleCount']));
      });
      _items = loadedProduct;
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  Future<void> addProduct(Product product) async {
    final url =
        'https://nomadio-19725-default-rtdb.asia-southeast1.firebasedatabase.app/products.json?auth=$authToken';
    try {
      final response = await http.post(
        Uri.parse(url),
        body: json.encode(
          {
            'title': product.title,
            'description': product.description,
            'imageUrl': product.imageUrl,
            'price': product.price,
            'classification': product.classification,
            'location': product.location,
            'vehicle': product.vehicle,
            'day': product.day,
            'peopleCount': product.peopleCount,
            'creatorId': userId,
          },
        ),
      );

      final newProduct = Product(
          id: json.decode(response.body)['name'],
          title: product.title,
          description: product.description,
          imageUrl: product.imageUrl,
          price: product.price,
          classification: product.classification,
          location: product.location,
          vehicle: product.vehicle,
          day: product.day,
          peopleCount: product.peopleCount);
      _items.add(newProduct);
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  Future<void> updatedPeopleCount(String id, int newProduct) async {
    final prodIndex = _items.indexWhere((prod) => prod.id == id);
    if (prodIndex >= 0) {
      final url =
          'https://nomadio-19725-default-rtdb.asia-southeast1.firebasedatabase.app/products/$id.json?auth=$authToken';
      await http.patch(Uri.parse(url),
          body: json.encode({'peopleCount': newProduct}));

      notifyListeners();
    } else {
      print("....");
    }
  }

  Future<void> updateProduct(String id, Product newProduct) async {
    final prodIndex = _items.indexWhere((prod) => prod.id == id);
    if (prodIndex >= 0) {
      final url =
          'https://nomadio-19725-default-rtdb.asia-southeast1.firebasedatabase.app/products/$id.json?auth=$authToken';
      await http.patch(Uri.parse(url),
          body: json.encode({
            'title': newProduct.title,
            'description': newProduct.description,
            'imageUrl': newProduct.imageUrl,
            'price': newProduct.price,
            'classification': newProduct.classification,
            'location': newProduct.location,
            'vehicle': newProduct.vehicle,
            'day': newProduct.day,
            'peopleCount': newProduct.peopleCount
          }));
      _items[prodIndex] = newProduct;
      notifyListeners();
    } else {
      print("....");
    }
  }

  Future<void> deleteProduct(String id) async {
    final url =
        'https://nomadio-19725-default-rtdb.asia-southeast1.firebasedatabase.app/products/$id.json?auth=$authToken';
    final existingProductIndex = _items.indexWhere((prod) => prod.id == id);
    Product? existingProduct = _items[existingProductIndex];
    _items.removeAt(existingProductIndex);
    notifyListeners();
    final response = await http.delete(Uri.parse(url));

    if (response.statusCode >= 400) {
      _items.insert(existingProductIndex, existingProduct);
      notifyListeners();
      throw HttpException('Устгах боломжгүй');
    }
    existingProduct = null;
  }
}
