import 'package:flutter/cupertino.dart';

import 'package:flutter/foundation.dart';

class CartItem {
  final String id;
  final String title;
  final int quantity;
  final double price;
  final String imageUrl;
  CartItem(
      {required this.id,
      required this.imageUrl,
      required this.price,
      required this.quantity,
      required this.title});
}

class Cart with ChangeNotifier {
  late Map<String, CartItem> _items = {};

  Map<String, CartItem> get items {
    return {..._items};
  }

  int get itemCount {
    return _items.length;
  }

  double get totalAmount {
    var total = 0.0;
    _items.forEach((key, cartItem) {
      total += cartItem.price * cartItem.quantity;
    });
    return total;
  }

  void addItem(String productId, double price, String title, int quantityCount,
      String imageUrl) {
    if (_items.containsKey(productId)) {
      _items.update(
        productId,
        (existingCartItem) => CartItem(
            imageUrl: existingCartItem.imageUrl,
            id: existingCartItem.id,
            price: existingCartItem.price,
            quantity: existingCartItem.quantity + quantityCount,
            title: existingCartItem.title),
      );
    } else {
      _items.putIfAbsent(
        productId,
        () => CartItem(
          imageUrl: imageUrl,
          id: DateTime.now().toString(),
          title: title,
          price: price,
          quantity: quantityCount,
        ),
      );
    }
    notifyListeners();
  }

  void removeItem(String productId) {
    _items.remove(productId);
    notifyListeners();
  }

  void removeSingleItem(String productId, int quantity) {
    if (!_items.containsKey(productId)) {
      return;
    }
    // if (_items[productId]!.quantity > 1) {
    //   print(_items[productId]!.quantity);
    //   _items.update(
    //     productId,
    //     (existingCartItem) => CartItem(
    //         id: existingCartItem.id,
    //         price: existingCartItem.price,
    //         quantity: existingCartItem.quantity - quantity,
    //         title: existingCartItem.title),
    //   );
    // }
    else {
      _items.remove(productId);
    }
    notifyListeners();
  }

  void clear() {
    _items = {};
    notifyListeners();
  }
}
