import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class Product with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;
  late final String classification;
  final String location;
  final String vehicle;
  final int day;
  int peopleCount = 0;
  bool isFavorite;
  bool internal;
  bool externals;
  Product(
      {required this.id,
      required this.title,
      required this.description,
      required this.imageUrl,
      required this.price,
      this.isFavorite = false,
      this.externals = false,
      this.internal = false,
      required this.classification,
      required this.location,
      required this.vehicle,
      required this.day,
      required this.peopleCount});

  void _setFavValue(bool newValue) {
    isFavorite = newValue;
    notifyListeners();
  }

  void setInternalValue(bool newIntValue) {
    internal = true;
    notifyListeners();
  }

  void setExternalValue(bool newExtValue) {
    externals = true;
    notifyListeners();
  }

  Future<void> toggleFavoriteStatus(String? token, String userId) async {
    final oldStatus = isFavorite;
    isFavorite = !isFavorite;
    notifyListeners();
    final url =
        'https://nomadio-19725-default-rtdb.asia-southeast1.firebasedatabase.app/userFavorites/$userId/$id.json?auth=$token';
    try {
      final response = await http.put(Uri.parse(url),
          body: json.encode(
            isFavorite,
          ));
      if (response.statusCode >= 400) {
        _setFavValue(oldStatus);
      }
    } catch (error) {
      _setFavValue(oldStatus);
    }
  }
}
