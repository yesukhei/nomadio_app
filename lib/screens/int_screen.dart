import 'package:flutter/material.dart';
import 'package:nomadio_app/providers/products.dart';
import 'package:nomadio_app/screens/edit_product_screen.dart';
import 'package:nomadio_app/widgets/product_item.dart';
import 'package:nomadio_app/widgets/user_product_item.dart';
import 'package:provider/provider.dart';

class InternalScreen extends StatelessWidget {
  final bool showInternal;
  static const routeName = '/user-products';
  InternalScreen(this.showInternal);
  Future<void> _refreshProducts(BuildContext context) async {
    await Provider.of<Products>(context, listen: false).fetchAndSetProducts();
  }

  @override
  Widget build(BuildContext context) {
    final productsData = Provider.of<Products>(context);
    final favItems =
        showInternal ? productsData.internalItems : productsData.items;
    return Scaffold(
      appBar: AppBar(
        title: Text('Дотоод аялалууд'),
      ),
      body: Column(children: <Widget>[
        // Center(
        //   child: Text("Хадгалсан аялалууд",
        //       style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
        // ),
        Expanded(
          child: GridView.builder(
            padding: const EdgeInsets.all(10.0),
            itemCount: favItems.length,
            itemBuilder: (ctx, i) => ChangeNotifierProvider.value(
              value: favItems[i],
              child: ProductItem(
                  //products[i].id, products[i].title, products[i].imageUrl
                  ),
            ),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 5 / 3.5,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10),
          ),
        ),
      ]),
    );
  }
}
