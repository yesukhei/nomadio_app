import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:nomadio_app/models/app_color.dart';
import 'package:nomadio_app/providers/auth.dart';
import 'package:nomadio_app/providers/cart.dart';
import 'package:nomadio_app/providers/product.dart';
import 'package:nomadio_app/providers/products.dart';
import 'package:provider/provider.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/cupertino.dart';

import 'cart_screen.dart';

class ProductDetailScreen extends StatefulWidget {
  // final String title;
  // ProductDetailScreen(this.title);
  static const routeName = './product-detail';

  @override
  State<ProductDetailScreen> createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  CarouselController buttonCarouselController = CarouselController();

  int _itemCount = 0;
  int dismissedPeopleCount = 0;

  @override
  Widget build(BuildContext context) {
    final productId = ModalRoute.of(context)!.settings.arguments as String;
    final loadedProduct = Provider.of<Products>(context).findById(productId);
    final cart = Provider.of<Cart>(context, listen: false);
    final authData = Provider.of<Auth>(context, listen: false);

    // return Scaffold(
    //   body: Container(
    //     height: double.infinity,
    //     child: Stack(
    //       children: <Widget>[
    //         Container(
    //           height: 300,
    //           alignment: Alignment.topCenter,
    //           decoration: BoxDecoration(
    //             image: DecorationImage(
    //                 image: NetworkImage(loadedProduct.imageUrl),
    //                 fit: BoxFit.fill),
    //           ),
    //           child: SafeArea(
    //             child: Row(
    //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //               children: <Widget>[
    //                 IconButton(
    //                   icon: Icon(Icons.arrow_back),
    //                   color: Colors.white,
    //                   onPressed: () {
    //                     Navigator.pop(context);
    //                   },
    //                 ),
    //                 IconButton(
    //                   color: Colors.white,
    //                   icon: Icon(loadedProduct.isFavorite
    //                       ? Icons.favorite
    //                       : Icons.favorite_border),
    //                   onPressed: () {
    //                     loadedProduct.toggleFavoriteStatus();
    //                   },
    //                 ),
    //               ],
    //             ),
    //           ),
    //         ),
    //         Positioned(
    //           top: 300,
    //           left: 0,
    //           right: 0,
    //           bottom: 0,
    //           child: Container(
    //               width: double.infinity,
    //               decoration: BoxDecoration(
    //                 borderRadius: BorderRadius.only(
    //                     topLeft: Radius.circular(28),
    //                     topRight: Radius.circular(28)),
    //               ),
    //               child: SingleChildScrollView(
    //                 child: Column(
    //                   children: <Widget>[
    //                     Padding(
    //                       padding: const EdgeInsets.all(24.0),
    //                       child: Row(
    //                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //                         crossAxisAlignment: CrossAxisAlignment.end,
    //                         children: <Widget>[
    //                           Text(
    //                             loadedProduct.title,
    //                             style: TextStyle(
    //                               fontSize: 22,
    //                               fontWeight: FontWeight.bold,
    //                             ),
    //                           ),
    //                           Text(
    //                             '${loadedProduct.price.toStringAsFixed(0)}\₮',
    //                             style: TextStyle(
    //                               fontSize: 22,
    //                               fontWeight: FontWeight.w500,
    //                             ),
    //                           ),
    //                         ],
    //                       ),
    //                     ),
    //                     Container(
    //                       margin: const EdgeInsets.symmetric(horizontal: 24),
    //                       padding: const EdgeInsets.symmetric(vertical: 16),
    //                       decoration: BoxDecoration(
    //                         border: Border.all(width: 1),
    //                         borderRadius: BorderRadius.circular(13),
    //                       ),
    //                       child: Row(
    //                         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    //                         children: <Widget>[
    //                           Column(
    //                             children: <Widget>[
    //                               Icon(
    //                                 Icons.location_pin,
    //                                 size: 30,
    //                               ),
    //                               SizedBox(
    //                                 height: 6,
    //                               ),
    //                               Text(
    //                                 loadedProduct.location,
    //                                 style: TextStyle(
    //                                   fontSize: 12,
    //                                 ),
    //                               )
    //                             ],
    //                           ),
    //                           Column(
    //                             children: <Widget>[
    //                               Icon(
    //                                 Icons.wb_sunny,
    //                                 size: 30,
    //                               ),
    //                               SizedBox(
    //                                 height: 6,
    //                               ),
    //                               Text(
    //                                 '${loadedProduct.day.toString()}\ өдөр',
    //                                 style: TextStyle(
    //                                   fontSize: 12,
    //                                 ),
    //                               ),
    //                             ],
    //                           ),
    //                           Column(
    //                             children: <Widget>[
    //                               Icon(
    //                                 Icons.airport_shuttle,
    //                                 size: 30,
    //                               ),
    //                               SizedBox(
    //                                 height: 6,
    //                               ),
    //                               Text(
    //                                 loadedProduct.vehicle,
    //                                 style: TextStyle(
    //                                   fontSize: 12,
    //                                 ),
    //                               )
    //                             ],
    //                           ),
    //                           Column(
    //                             children: <Widget>[
    //                               Icon(
    //                                 Icons.class_,
    //                                 size: 30,
    //                               ),
    //                               SizedBox(
    //                                 height: 6,
    //                               ),
    //                               Text(
    //                                 loadedProduct.classification,
    //                                 style: TextStyle(
    //                                   fontSize: 12,
    //                                 ),
    //                               )
    //                             ],
    //                           ),
    //                         ],
    //                       ),
    //                     ),
    //                     Container(
    //                       padding: const EdgeInsets.all(24),
    //                       child: Text(
    //                         loadedProduct.description,
    //                         style: TextStyle(
    //                             height: 2, fontSize: 12, letterSpacing: 1.2),
    //                       ),
    //                     ),
    //                     Row(
    //                       mainAxisAlignment: MainAxisAlignment.center,
    //                       children: <Widget>[
    //                         _itemCount != 0
    //                             ? IconButton(
    //                                 icon: new Icon(Icons.remove),
    //                                 onPressed: () {
    //                                   setState(() {
    //                                     _itemCount--;
    //                                   });
    //                                 })
    //                             : Container(),
    //                         Text(_itemCount.toString()),
    //                         IconButton(
    //                             icon: new Icon(Icons.add),
    //                             onPressed: () {
    //                               setState(() {
    //                                 _itemCount++;
    //                               });
    //                             }),
    //                       ],
    //                     ),
    //                     Column(
    //                       children: <Widget>[
    //                         Container(
    //                           margin: const EdgeInsets.symmetric(
    //                               horizontal: 24, vertical: 16),
    //                           child: ClipRRect(
    //                             borderRadius: BorderRadius.circular(13),
    //                             child: FlatButton(
    //                               onPressed: () {
    //                                 cart.addItem(
    //                                   loadedProduct.id,
    //                                   loadedProduct.price,
    //                                   loadedProduct.title,
    //                                   _itemCount,
    //                                 );

    //                                 ScaffoldMessenger.of(context)
    //                                     .hideCurrentSnackBar();
    //                                 ScaffoldMessenger.of(context).showSnackBar(
    //                                   SnackBar(
    //                                     content: Text("Бараа нэмэгдлээ"),
    //                                     duration: Duration(seconds: 2),
    //                                     action: SnackBarAction(
    //                                       label: 'Болих',
    //                                       onPressed: () {
    //                                         cart.removeSingleItem(
    //                                             loadedProduct.id);
    //                                       },
    //                                     ),
    //                                   ),
    //                                 );
    //                               },
    //                               color: Colors.blueAccent,
    //                               child: Stack(
    //                                 alignment: Alignment.center,
    //                                 children: <Widget>[
    //                                   Container(
    //                                     alignment: Alignment.center,
    //                                     width: double.infinity,
    //                                     height: 46,
    //                                     child: Text(
    //                                       'Сагсанд нэмэх',
    //                                       style: TextStyle(
    //                                         fontSize: 16,
    //                                       ),
    //                                     ),
    //                                   ),
    //                                   Positioned(
    //                                     right: 16,
    //                                     child: Icon(Icons.shopping_cart),
    //                                   ),
    //                                 ],
    //                               ),
    //                             ),
    //                           ),
    //                         ),
    //                       ],
    //                     ),
    //                   ],
    //                 ),
    //               )),
    //         ),
    //       ],
    //     ),
    //   ),
    // );
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Stack(
        children: [
          Container(
              height: height / 2.2,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(loadedProduct.imageUrl),
                    fit: BoxFit.cover),
              )),
          Positioned(
            top: 70.0,
            left: 20.0,
            child: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                height: 40.0,
                width: 40.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Color(0xFFFFFFFF),
                ),
                child: Icon(Icons.arrow_back),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: height / 2.4),
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            height: height,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30.0),
                topRight: Radius.circular(30.0),
              ),
              color: Color(0xFFFFFFFF),
            ),
            child: ListView(
              physics: BouncingScrollPhysics(),
              padding: EdgeInsets.only(top: 20.0),
              children: [
                Text(
                  loadedProduct.title,
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 5.0),
                Text(
                  loadedProduct.location,
                  style: TextStyle(
                    color: Color(0xFF939498),
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(height: 20.0),
                Row(
                  children: [
                    Container(
                      height: 25.0,
                      padding:
                          EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                      margin: EdgeInsets.only(right: 10.0),
                      color: Color(0xFF939498).withOpacity(0.2),
                      child: Text("Аялалын тайлбар"),
                    ),
                  ],
                ),
                SizedBox(height: 20.0),
                Text(
                  loadedProduct.description,
                  style: TextStyle(
                    color: Color(0xFF939498),
                    fontWeight: FontWeight.w400,
                  ),
                ),
                Divider(
                  thickness: 1,
                  height: 40.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(width: 1),
                    Column(
                      children: <Widget>[
                        Icon(
                          Icons.location_pin,
                          size: 30,
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Text(
                          loadedProduct.location,
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        )
                      ],
                    ),
                    SizedBox(width: 10),
                    Column(
                      children: <Widget>[
                        Icon(
                          Icons.wb_sunny,
                          size: 30,
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Text(
                          "${loadedProduct.day.toString()}\ өдөр",
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        )
                      ],
                    ),
                    SizedBox(width: 10),
                    Column(
                      children: <Widget>[
                        Icon(
                          Icons.class_,
                          size: 30,
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Text(
                          loadedProduct.classification,
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      children: <Widget>[
                        Icon(
                          Icons.bus_alert,
                          size: 30,
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Text(
                          loadedProduct.vehicle,
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        )
                      ],
                    ),
                    SizedBox(width: 1),
                  ],
                ),
                Divider(
                  thickness: 1,
                  height: 40.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Хүний тоо',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      width: 50,
                    ),
                    _itemCount != 0
                        ? IconButton(
                            icon: new Icon(Icons.remove),
                            onPressed: () {
                              setState(() {
                                _itemCount--;
                              });
                            })
                        : Container(),
                    Text(_itemCount.toString(),
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold)),
                    _itemCount == loadedProduct.peopleCount
                        ? Container()
                        : IconButton(
                            icon: new Icon(Icons.add),
                            onPressed: () {
                              setState(() {
                                _itemCount++;
                              });
                            })
                  ],
                ),
                SizedBox(height: 10.0),
                Center(
                  child: Text(
                    'Үлдэгдэл ${loadedProduct.peopleCount.toString()}',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Color(0xFF939498),
                    ),
                  ),
                ),
                SizedBox(height: 50.0),
              ],
            ),
          ),
          Positioned(
            top: height / 2.55,
            right: 20.0,
            child: Container(
              height: 40.0,
              width: 40.0,
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    blurRadius: 20.0,
                    offset: Offset(0, 10),
                    color: Colors.black.withOpacity(0.15),
                  ),
                ],
                color: Color(0xFFFFFFFF),
                borderRadius: BorderRadius.circular(40.0),
              ),
              child: IconButton(
                onPressed: () {
                  loadedProduct.toggleFavoriteStatus(
                      authData.token, authData.userId);
                },
                color: Colors.red,
                icon: Icon(loadedProduct.isFavorite
                    ? Icons.favorite
                    : Icons.favorite_border),
              ),
            ),
          )
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 60.0,
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                blurRadius: 20.0,
                offset: Offset(0, 5),
                color: Colors.black.withOpacity(0.15),
              ),
            ],
            color: Color(0xFFFFFFFF),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    loadedProduct.price.toString(),
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      fontSize: 20.0,
                      color: Color(0xFF939498),
                    ),
                  ),
                  Text(
                    '1 хүний үнэ',
                  ),
                ],
              ),
              FlatButton(
                onPressed: () {
                  if (_itemCount == 0) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text("Хүний тоогоо сонгоно уу !"),
                        duration: Duration(seconds: 2),
                      ),
                    );
                  } else if (_itemCount > loadedProduct.peopleCount) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text("Хүний тоо дүүрсэн"),
                        duration: Duration(seconds: 2),
                      ),
                    );
                  } else {
                    final updatedPeopleCount =
                        loadedProduct.peopleCount - _itemCount;
                    print(updatedPeopleCount);
                    Provider.of<Products>(context, listen: false)
                        .updatedPeopleCount(
                            loadedProduct.id, updatedPeopleCount)
                        .then((_) =>
                            Provider.of<Products>(context, listen: false)
                                .fetchAndSetProducts());

                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text("Аялал нэмэгдлээ"),
                        duration: Duration(seconds: 2),
                        action: SnackBarAction(
                          label: 'Болих',
                          onPressed: () async {
                            final dismissedPeopleCount =
                                updatedPeopleCount + _itemCount;
                            print(dismissedPeopleCount);
                            await Provider.of<Products>(context, listen: false)
                                .updatedPeopleCount(
                                    loadedProduct.id, dismissedPeopleCount)
                                .then((_) => cart.removeSingleItem(
                                    loadedProduct.id, _itemCount))
                                .then((_) => Provider.of<Products>(context,
                                        listen: false)
                                    .fetchAndSetProducts());
                            // Navigator.of(context).pushNamed(
                            //     CartScreen.routeName,
                            //     arguments: dismissedPeopleCount);
                          },
                        ),
                      ),
                    );
                  }
                  _itemCount == 0 || _itemCount > loadedProduct.peopleCount
                      ? null
                      : cart.addItem(
                          loadedProduct.id,
                          loadedProduct.price,
                          loadedProduct.title,
                          _itemCount,
                          loadedProduct.imageUrl);
                },
                child: Container(
                  height: 40.0,
                  padding: EdgeInsets.symmetric(horizontal: 30.0),
                  decoration: BoxDecoration(
                    color: Color(0xFF2BA4F9),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Center(
                    child: Text(
                      'Сагсанд нэмэх',
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 16.0,
                        color: Color(0xFFFFFFFF),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
