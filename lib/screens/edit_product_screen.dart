import 'package:flutter/material.dart';
import 'package:nomadio_app/providers/product.dart';
import 'package:nomadio_app/providers/products.dart';
import 'package:provider/provider.dart';

class EditProductScreen extends StatefulWidget {
  static const routeName = '/edit-product';
  const EditProductScreen({Key? key}) : super(key: key);

  @override
  _EditProductScreenState createState() => _EditProductScreenState();
}

class _EditProductScreenState extends State<EditProductScreen> {
  String dropdownValue = 'Gadaad';
  final _priceFocusNode = FocusNode();
  final _descriptionFocusNode = FocusNode();
  final _imageUrlController = TextEditingController();
  final _imageUrlFocusNode = FocusNode();
  final _form = GlobalKey<FormState>();
  var _editedProduct = Product(
      id: '',
      title: '',
      description: '',
      imageUrl: '',
      price: 0,
      classification: '',
      location: '',
      vehicle: '',
      day: 0,
      peopleCount: 1);
  var _isInit = true;
  var _initValues = {
    'title': '',
    'description': '',
    'price': '',
    'imageUrl': '',
    'classificatio': '',
    'location': '',
    'vehicle': '',
    'day': '',
    'peopleCount': ''
  };
  var _isLoading = false;
  @override
  void initState() {
    _imageUrlFocusNode.addListener(_updateImageUrl);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      final productId = ModalRoute.of(context)!.settings.arguments;
      if (productId != null) {
        _editedProduct =
            Provider.of<Products>(context).findById(productId.toString());
        _initValues = {
          'title': _editedProduct.title,
          'description': _editedProduct.description,
          'price': _editedProduct.price.toString(),
          // 'imageUrl': _editedProduct.imageUrl,
          'imageUrl': '',
          'classification': _editedProduct.classification,
          'location': _editedProduct.location,
          'vehicle': _editedProduct.vehicle,
          'day': _editedProduct.day.toString(),
          'peopleCount': _editedProduct.peopleCount.toString()
        };
        _imageUrlController.text = _editedProduct.imageUrl;
      }
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _imageUrlFocusNode.removeListener(_updateImageUrl);
    _priceFocusNode.dispose();
    _descriptionFocusNode.dispose();
    _imageUrlController.dispose();
    _imageUrlFocusNode.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  void _updateImageUrl() {
    if (!_imageUrlFocusNode.hasFocus) {
      if (_imageUrlController.text.isEmpty ||
          (!_imageUrlController.text.startsWith('http') &&
              !_imageUrlController.text.startsWith('https'))) {
        return;
      }
      setState(() {});
    }
  }

  Future<void> _saveForm() async {
    final isValid = _form.currentState!.validate();
    if (!isValid) {
      return;
    }
    _form.currentState!.save();
    setState(() {
      _isLoading = true;
    });

    if (_editedProduct.id != '') {
      await Provider.of<Products>(context, listen: false)
          .updateProduct(_editedProduct.id, _editedProduct);
    } else {
      try {
        await Provider.of<Products>(context, listen: false)
            .addProduct(_editedProduct);
      } catch (error) {
        await showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text("Алдаа гарлаа"),
            content: Text(error.toString()),
            actions: <Widget>[
              FlatButton(
                child: Text("Okay"),
                onPressed: () {
                  Navigator.of(ctx).pop();
                },
              ),
            ],
          ),
        );
      }
      // finally {
      //   setState(() {
      //     _isLoading = false;
      //   });
      //   Navigator.of(context).pop();
      // }
    }
    setState(() {
      _isLoading = false;
    });
    Navigator.of(context).pop();
    //Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Product'),
        actions: <Widget>[
          IconButton(
            onPressed: _saveForm,
            icon: Icon(Icons.save),
          ),
        ],
      ),
      body: _isLoading
          ? Center(child: CircularProgressIndicator())
          : Padding(
              padding: const EdgeInsets.all(16.0),
              child: Form(
                key: _form,
                child: ListView(
                  children: <Widget>[
                    TextFormField(
                      initialValue: _initValues['title'],
                      decoration: InputDecoration(labelText: 'Нэр'),
                      textInputAction: TextInputAction.next,
                      onFieldSubmitted: (_) {
                        FocusScope.of(context).requestFocus(_priceFocusNode);
                      },
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Хоосон утга';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        _editedProduct = Product(
                            id: _editedProduct.id,
                            isFavorite: _editedProduct.isFavorite,
                            title: value!,
                            description: _editedProduct.description,
                            imageUrl: _editedProduct.imageUrl,
                            price: _editedProduct.price,
                            classification: _editedProduct.classification,
                            location: _editedProduct.location,
                            vehicle: _editedProduct.vehicle,
                            day: _editedProduct.day,
                            peopleCount: _editedProduct.peopleCount);
                      },
                    ),
                    TextFormField(
                      initialValue: _initValues['price'],
                      decoration: InputDecoration(labelText: 'Үнэ'),
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.number,
                      focusNode: _priceFocusNode,
                      onSaved: (value) {
                        _editedProduct = Product(
                            id: _editedProduct.id,
                            isFavorite: _editedProduct.isFavorite,
                            title: _editedProduct.title,
                            description: _editedProduct.description,
                            imageUrl: _editedProduct.imageUrl,
                            classification: _editedProduct.classification,
                            location: _editedProduct.location,
                            vehicle: _editedProduct.vehicle,
                            price: double.parse(value!),
                            day: _editedProduct.day,
                            peopleCount: _editedProduct.peopleCount);
                      },
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Үнээ оруул';
                        }
                        if (double.tryParse(value) == null) {
                          return 'Зөв утга оруул';
                        }
                        if (double.parse(value) <= 0) {
                          return '0-ээс их утга оруул';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      initialValue: _initValues['description'],
                      decoration: InputDecoration(labelText: 'Тайлбар'),
                      maxLines: 3,
                      keyboardType: TextInputType.multiline,
                      focusNode: _descriptionFocusNode,
                      onFieldSubmitted: (_) {
                        FocusScope.of(context)
                            .requestFocus(_descriptionFocusNode);
                      },
                      onSaved: (value) {
                        _editedProduct = Product(
                            classification: _editedProduct.classification,
                            location: _editedProduct.location,
                            vehicle: _editedProduct.vehicle,
                            id: _editedProduct.id,
                            isFavorite: _editedProduct.isFavorite,
                            title: _editedProduct.title,
                            description: value!,
                            imageUrl: _editedProduct.imageUrl,
                            price: _editedProduct.price,
                            day: _editedProduct.day,
                            peopleCount: _editedProduct.peopleCount);
                      },
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Тайлбараа оруул';
                        }
                        if (value.length < 10) {
                          return '10-аас их тэмдэгт оруул';
                        }
                        return null;
                      },
                    ),
                    DropdownButton<String>(
                      value: dropdownValue,
                      icon: const Icon(Icons.arrow_downward),
                      elevation: 16,
                      style: const TextStyle(color: Colors.deepPurple),
                      underline: Container(
                        height: 2,
                        color: Colors.deepPurpleAccent,
                      ),
                      onChanged: (value) {
                        _editedProduct = Product(
                            id: _editedProduct.id,
                            isFavorite: _editedProduct.isFavorite,
                            title: _editedProduct.title,
                            description: _editedProduct.description,
                            imageUrl: _editedProduct.imageUrl,
                            price: _editedProduct.price,
                            classification: value!,
                            location: _editedProduct.location,
                            vehicle: _editedProduct.vehicle,
                            day: _editedProduct.day,
                            peopleCount: _editedProduct.peopleCount);
                      },
                      items: <String>['Gadaad', 'Dotood']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                    // TextFormField(
                    //   initialValue: _initValues['classification'],
                    //   decoration: InputDecoration(labelText: 'ангилал'),
                    //   textInputAction: TextInputAction.next,
                    //   onFieldSubmitted: (_) {
                    //     FocusScope.of(context).requestFocus(_priceFocusNode);
                    //   },
                    //   // validator: (value) {
                    //   //   if (value!.isEmpty) {
                    //   //     return 'Хоосон утга';
                    //   //   }
                    //   //   return null;
                    //   // },
                    //   onSaved: (value) {
                    //     _editedProduct = Product(
                    //         id: _editedProduct.id,
                    //         isFavorite: _editedProduct.isFavorite,
                    //         title: _editedProduct.title,
                    //         description: _editedProduct.description,
                    //         imageUrl: _editedProduct.imageUrl,
                    //         price: _editedProduct.price,
                    //         classification: value!,
                    //         location: _editedProduct.location,
                    //         vehicle: _editedProduct.vehicle,
                    //         day: _editedProduct.day,
                    //         peopleCount: _editedProduct.peopleCount);
                    //   },
                    // ),
                    TextFormField(
                      initialValue: _initValues['location'],
                      decoration: InputDecoration(labelText: 'байршил'),
                      textInputAction: TextInputAction.next,
                      onFieldSubmitted: (_) {
                        FocusScope.of(context).requestFocus(_priceFocusNode);
                      },
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Хоосон утга';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        _editedProduct = Product(
                            id: _editedProduct.id,
                            isFavorite: _editedProduct.isFavorite,
                            title: _editedProduct.title,
                            description: _editedProduct.description,
                            imageUrl: _editedProduct.imageUrl,
                            price: _editedProduct.price,
                            classification: _editedProduct.classification,
                            location: value!,
                            vehicle: _editedProduct.vehicle,
                            day: _editedProduct.day,
                            peopleCount: _editedProduct.peopleCount);
                      },
                    ),
                    TextFormField(
                      initialValue: _initValues['vehicle'],
                      decoration:
                          InputDecoration(labelText: 'тээврийн хэрэгсэл'),
                      textInputAction: TextInputAction.next,
                      onFieldSubmitted: (_) {
                        FocusScope.of(context).requestFocus(_priceFocusNode);
                      },
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Хоосон утга';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        _editedProduct = Product(
                            id: _editedProduct.id,
                            isFavorite: _editedProduct.isFavorite,
                            title: _editedProduct.title,
                            description: _editedProduct.description,
                            imageUrl: _editedProduct.imageUrl,
                            price: _editedProduct.price,
                            classification: _editedProduct.classification,
                            location: _editedProduct.location,
                            vehicle: value!,
                            day: _editedProduct.day,
                            peopleCount: _editedProduct.peopleCount);
                      },
                    ),
                    TextFormField(
                      initialValue: _initValues['day'],
                      decoration: InputDecoration(labelText: 'Үргэлжлэх өдөр'),
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.number,
                      //focusNode: _priceFocusNode,
                      onSaved: (value) {
                        _editedProduct = Product(
                            id: _editedProduct.id,
                            isFavorite: _editedProduct.isFavorite,
                            title: _editedProduct.title,
                            description: _editedProduct.description,
                            imageUrl: _editedProduct.imageUrl,
                            classification: _editedProduct.classification,
                            location: _editedProduct.location,
                            vehicle: _editedProduct.vehicle,
                            price: _editedProduct.price,
                            day: int.parse(value!),
                            peopleCount: _editedProduct.peopleCount);
                      },
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'утга оруул';
                        }
                        if (double.tryParse(value) == null) {
                          return 'Зөв утга оруул';
                        }
                        if (double.parse(value) <= 0) {
                          return '0-ээс их утга оруул';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      initialValue: _initValues['peopleCount'],
                      decoration: InputDecoration(labelText: 'Хүний тоо'),
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.number,
                      //focusNode: _priceFocusNode,
                      onSaved: (value) {
                        _editedProduct = Product(
                            id: _editedProduct.id,
                            isFavorite: _editedProduct.isFavorite,
                            title: _editedProduct.title,
                            description: _editedProduct.description,
                            imageUrl: _editedProduct.imageUrl,
                            classification: _editedProduct.classification,
                            location: _editedProduct.location,
                            vehicle: _editedProduct.vehicle,
                            price: _editedProduct.price,
                            day: _editedProduct.day,
                            peopleCount: int.parse(value!));
                      },
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'утга оруул';
                        }
                        if (double.tryParse(value) == null) {
                          return 'Зөв утга оруул';
                        }
                        if (double.parse(value) <= 0) {
                          return '0-ээс их утга оруул';
                        }
                        return null;
                      },
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          width: 100,
                          height: 100,
                          margin: EdgeInsets.only(top: 8, right: 10),
                          decoration: BoxDecoration(
                            border: Border.all(width: 1, color: Colors.grey),
                          ),
                          child: _imageUrlController.text.isEmpty
                              ? Text('Enter URL')
                              : FittedBox(
                                  child:
                                      Image.network(_imageUrlController.text),
                                  fit: BoxFit.cover,
                                ),
                        ),
                        Expanded(
                          child: TextFormField(
                            decoration: InputDecoration(labelText: 'Image URL'),
                            keyboardType: TextInputType.url,
                            textInputAction: TextInputAction.done,
                            controller: _imageUrlController,
                            focusNode: _imageUrlFocusNode,
                            onFieldSubmitted: (_) {
                              _saveForm();
                            },
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Зургийн линк оруулна уу';
                              }
                              if (!value.startsWith('http') &&
                                  !value.startsWith('https')) {
                                return 'Зөв URL хаяг оруулна уу !';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              _editedProduct = Product(
                                  classification: _editedProduct.classification,
                                  location: _editedProduct.location,
                                  vehicle: _editedProduct.vehicle,
                                  id: _editedProduct.id,
                                  isFavorite: _editedProduct.isFavorite,
                                  title: _editedProduct.title,
                                  description: _editedProduct.description,
                                  imageUrl: value!,
                                  price: _editedProduct.price,
                                  day: _editedProduct.day,
                                  peopleCount: _editedProduct.peopleCount);
                            },
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
    );
  }
}
