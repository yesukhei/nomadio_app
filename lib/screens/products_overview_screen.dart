import 'package:flutter/material.dart';
import 'package:nomadio_app/providers/auth.dart';
import 'package:nomadio_app/providers/cart.dart';
import 'package:nomadio_app/providers/products.dart';
import 'package:nomadio_app/screens/cart_screen.dart';
import 'package:nomadio_app/screens/favorites_screen.dart';
import 'package:nomadio_app/screens/orders_screen.dart';
import 'package:nomadio_app/screens/user_products_screen.dart';
import 'package:nomadio_app/widgets/badge.dart';
import 'package:nomadio_app/widgets/products_grid.dart';
import 'package:provider/provider.dart';
import '../providers/product.dart';
import '../widgets/product_item.dart';

enum FilterOptions {
  Internal,
  External,
  Favorites,
  All,
}

class ProductsOverviewScreen extends StatefulWidget {
  @override
  State<ProductsOverviewScreen> createState() => _ProductsOverviewScreenState();
}

class _ProductsOverviewScreenState extends State<ProductsOverviewScreen> {
  var _showOnlyFavorites = false;
  var _showExternalItems = false;
  var _showInternalItems = false;
  var _isInit = true;
  var _isLoading = false;
  void initState() {
    // Provider.of<Products>(context).fetchAndSetProducts();

    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<Products>(context).fetchAndSetProducts().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final internalData = Provider.of<Products>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Nomadio",
        ),
        centerTitle: true,
        actions: <Widget>[
          Consumer<Cart>(
            builder: (_, cart, ch) => Badge(
              child: ch!,
              value: cart.itemCount.toString(),
              color: Colors.red,
            ),
            child: IconButton(
              icon: Icon(Icons.shopping_cart),
              onPressed: () {
                Navigator.of(context).pushNamed(CartScreen.routeName);
              },
            ),
          )
        ],
      ),
      drawer: Drawer(
        child: Column(
          children: [
            Container(
              child: Padding(
                padding: EdgeInsets.only(top: 50.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: const [
                    CircleAvatar(
                      radius: 50.0,
                      backgroundImage: NetworkImage(
                        "https://e7.pngegg.com/pngimages/558/876/png-clipart-earth-globe-computer-icons-earth-globe-logo.png",
                      ),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text(
                      "Nomadio",
                      style: TextStyle(
                        fontSize: 22.0,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text(
                      "Travel Planner App",
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 20.0,
            ),
            //Now let's Add the button for the Menu
            //and let's copy that and modify it

            ListTile(
              onTap: () {
                Navigator.of(context).pushNamed(UserProductsScreen.routeName);
              },
              leading: const Icon(
                Icons.edit,
                color: Colors.black,
              ),
              title: const Text("Аялалыг удирдах"),
            ),

            ListTile(
              onTap: () {
                Navigator.of(context).pushNamed(OrdersScreen.routeName);
              },
              leading: const Icon(
                Icons.assessment,
                color: Colors.black,
              ),
              title: const Text("Таны захиалгууд"),
            ),

            ListTile(
              onTap: () {
                setState(() {
                  _showOnlyFavorites = true;
                });
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => FavoriteScreen(_showOnlyFavorites)),
                );
              },
              leading: const Icon(
                Icons.favorite_outline_outlined,
                color: Colors.black,
              ),
              title: const Text("Таалагдсан"),
            ),
            ListTile(
              onTap: () {
                Provider.of<Auth>(context, listen: false).logout();
              },
              leading: const Icon(
                Icons.logout,
                color: Colors.black,
              ),
              title: const Text("Гарах"),
            ),
          ],
        ),
      ),
      body: _isLoading
          ? Center(child: CircularProgressIndicator())
          : ProductsGrid(_showOnlyFavorites),
    );
  }
}
