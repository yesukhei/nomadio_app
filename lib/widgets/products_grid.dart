import 'package:flutter/material.dart';
import 'package:nomadio_app/providers/products.dart';
import 'package:nomadio_app/screens/ext.screen.dart';
import 'package:nomadio_app/screens/int_screen.dart';
import '../providers/product.dart';
import './product_item.dart';
import 'package:provider/provider.dart';
import '../providers/products.dart';

class ProductsGrid extends StatefulWidget {
  final bool showOnlyFavorites;

  ProductsGrid(this.showOnlyFavorites);

  @override
  State<ProductsGrid> createState() => _ProductsGridState();
}

class _ProductsGridState extends State<ProductsGrid> {
  bool _isSearched = false;
  int _showExt = 0;

  final _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final productsData = Provider.of<Products>(context);

    final recommItems = _isSearched
        ? productsData.searchItems(_controller.text)
        : _showExt == 0
            ? productsData.items
            : _showExt == 1
                ? productsData.externalItems
                : _showExt == 2
                    ? productsData.internalItems
                    : null;

    return Column(children: <Widget>[
      Center(
        child: Column(
          children: [
            Container(
              height: 42,
              margin: const EdgeInsets.fromLTRB(16, 16, 16, 16),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Colors.white,
                border: Border.all(color: Colors.black26),
              ),
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: TextField(
                  controller: _controller,
                  decoration: const InputDecoration(
                    hintText: "Байршил",
                    icon: Icon(Icons.search, color: Colors.black),
                  ),
                  onChanged: (_) {
                    Provider.of<Products>(context, listen: false)
                        .searchItems(_controller.text);
                    setState(() {
                      _isSearched = true;
                    });
                  }),
            )
          ],
        ),
      ),
      Row(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              onPressed: () {
                setState(() {
                  _showExt = 0;
                });
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //       builder: (context) => ExternalScreen(_showExt)),
                // );
              },
              child: Text("Бүх аялал"),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              onPressed: () {
                setState(() {
                  _showExt = 1;
                });
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //       builder: (context) => ExternalScreen(_showExt)),
                // );
              },
              child: Text("Гадаад аялал"),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              onPressed: () {
                setState(() {
                  _showExt = 2;
                });
              },
              //   Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //         builder: (context) => InternalScreen(_showInt)),
              //   );
              // },
              child: Text("Дотоод аялал"),
            ),
          ),
        ],
      ),
      Expanded(
        child: GridView.builder(
          scrollDirection: Axis.vertical,
          padding: const EdgeInsets.all(10.0),
          itemCount: recommItems!.length,
          itemBuilder: (ctx, i) => ChangeNotifierProvider.value(
            value: recommItems[i],
            child: ProductItem(
                //products[i].id, products[i].title, products[i].imageUrl
                ),
          ),
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: 3 / 2,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10),
        ),
      ),
      // Center(
      //   child: Text("Дотоод аялалууд",
      //       style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      // ),
      // Expanded(
      //   child: GridView.builder(
      //     scrollDirection: Axis.horizontal,
      //     padding: const EdgeInsets.all(10.0),
      //     itemCount: internalItems.length,
      //     itemBuilder: (ctx, i) => ChangeNotifierProvider.value(
      //       value: internalItems[i],
      //       child: ProductItem(
      //           //products[i].id, products[i].title, products[i].imageUrl
      //           ),
      //     ),
      //     gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
      //         crossAxisCount: 1,
      //         childAspectRatio: 1.5 / 2,
      //         crossAxisSpacing: 10,
      //         mainAxisSpacing: 10),
      //   ),
      // ),
      // Center(
      //   child: Text("Гадаад аялалууд",
      //       style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      // ),
      // Expanded(
      //   child: GridView.builder(
      //     scrollDirection: Axis.horizontal,
      //     padding: const EdgeInsets.all(10.0),
      //     itemCount: externalItems.length,
      //     itemBuilder: (ctx, i) => ChangeNotifierProvider.value(
      //       value: externalItems[i],
      //       child: ProductItem(
      //           //products[i].id, products[i].title, products[i].imageUrl
      //           ),
      //     ),
      //     gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
      //         crossAxisCount: 1,
      //         childAspectRatio: 1.5 / 2,
      //         crossAxisSpacing: 10,
      //         mainAxisSpacing: 10),
      //   ),
      // ),
    ]);
  }
}
